package lesson1.visibility;


class A {
   public int f1;
   protected int f2;
   int f3;
   private int f4;

   public void m1() {
   }

   protected void m2() {
   }

   void m3() {
   }

   private void m4() {
   }
}

class B extends A {
   B() {
      this.f1 = 0; // visible since public
      this.f2 = 0; // visible because extending
      this.f3 = 0; // visible because same package
//      this.f4 = 0; // private in accessible

      this.m1(); // visible since public
      this.m2(); // visible because extending
      this.m3(); // visible because same package
//      this.m4(); // private in accessible
   }
}